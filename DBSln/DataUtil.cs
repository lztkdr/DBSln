﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DBSln
{
    public class DataUtil
    {

        public static List<T> GetFirstCol<T>(DataTable data)
        {
            List<T> lst = new List<T>();
            if (data == null || data.Rows.Count <= 0)
            {
                return lst;
            }
            else
            {
                foreach (DataRow dr in data.Rows)
                {
                    if (dr[0] != null)
                    {
                        T t = (T)Convert.ChangeType(dr[0], typeof(T));
                        lst.Add(t);
                    }
                }
            }
            return lst;
        }

        public static List<string> GetAllColumnNames(DataTable dt)
        {
            List<string> lst = new List<string>();
            if (dt != null && dt.Columns.Count > 0)
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    lst.Add(dc.ColumnName);
                }
            }
            return lst;
        }
        public static string SQLIn<T>(string field, bool isNot = false, params T[] members)
        {
            string result = string.Empty;
            if (members == null || members.Length <= 0)
            {
                return string.Empty;
            }
            List<string> lst = new List<string>();
            foreach (T obj in members)
            {

                if (obj != null)
                {
                    string val = obj.ToString();
                    if (val.Contains("'"))
                    {
                        continue;
                    }
                    lst.Add("'" + obj.ToString() + "'");
                }
            }
            result = " and " + field + " " + (isNot ? "not" : "") + " in (" + string.Join(",", lst) + ") ";
            return result;
        }
    }
}
