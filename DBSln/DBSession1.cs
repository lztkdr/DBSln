﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBSln.Helper;

namespace DBSln
{

    public class DBSession1
    {

        private string connstrings = string.Empty;
        private DBHelper dbhelper = null;
        public DBHelper DBSession(DataBaseType type)
        {
            switch (type)
            {
                case DataBaseType.MsSql:
                    dbhelper = new DbHelperMsSql();
                    break;
                case DataBaseType.MySql:
                    break;
                case DataBaseType.Oracle:
                    break;
                case DataBaseType.OracleManaged:
                    break;
                case DataBaseType.Sqlite3:
                    break;
                case DataBaseType.Oledb:
                default:
                    break;
            }
            return dbhelper;
        }

        public DBSession(DataBaseType type, string connstrings)
        {
            this.connstrings = connstrings;
        }

    }

    public enum DataBaseType
    {
        MsSql,
        MySql,
        Oracle,
        OracleManaged,
        Sqlite3,
        Oledb
    }
}
