﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DBUtility
{

   

    /// <summary>
    /// 有关SQL语句字符串操作处理类
    /// </summary>
    public class DBSession
    {

        private static DbHelper dbhelper = null;
        public DBSession(DataBaseType dbtype)
        {

        }



    }

    public enum DataBaseType
    {
        MSSql,
        MySql,
        Oracle,
        OracleManaged,
        Sqlite3,
        Oledb
    }

    public abstract class DbHelper
    {

        public IDbConnection CreateConnection(string connectionString, string dbProviderName)
        {
            DbProviderFactory dbfactory = DbProviderFactories.GetFactory(dbProviderName);
            IDbConnection dbconn = dbfactory.CreateConnection();
            dbconn.ConnectionString = connectionString;
            return dbconn;
        }
        public IDbConnection GetProviderName(DataBaseType type)
        {
            string dbProviderName = "System.Data.SqlClient";
            switch (type)
            {
                case DataBaseType.MSSql:
                    dbProviderName = "System.Data.SqlClient";
                    break;
                case DataBaseType.MySql:
                    dbProviderName = "MySql.Data.MySqlClient";
                    break;
                case DataBaseType.Oracle:
                    dbProviderName = "System.Data.OracleClient";
                    break;
                case DataBaseType.OracleManaged:
                    dbProviderName = "Oracle.DataAccess.Client";
                    break;
                case DataBaseType.Sqlite3:
                    dbProviderName = "System.Data.SQLite";
                    break;
                case DataBaseType.Oledb:
                    dbProviderName = "System.Data.OleDb";
                    break;
                default:
                    break;
            }
            DbProviderFactory dbfactory = DbProviderFactories.GetFactory(dbProviderName);
            IDbConnection dbconn = dbfactory.CreateConnection();
            dbconn.ConnectionString = ConfigurationManager.ConnectionStrings[dbProviderName].ConnectionString;
            return dbconn;
        }
        
        public abstract Dictionary<string, string> GetTables();

        public abstract List<string> GetAllColumnNames(string tableName);

        public abstract bool TabExists(string tableName);

        public abstract bool Exists(string strSql, params IDataParameter[] cmdParms);

        public abstract T GetSingle<T>(string strSql, int Times = 30, params IDataParameter[] cmdParms);

        public abstract DataRow QueryRow(string strSql, int Times = 30, params IDataParameter[] cmdParms);

        public abstract DataTable QueryTable(string strSql, int Times = 30, params IDataParameter[] cmdParms);

        public abstract DataSet QueryDataSet(string SQLString, int Times = 30, params IDataParameter[] cmdParms);

        public virtual int ExecuteSql(string SQLString, int Times = 30, params IDataParameter[] cmdParms)
        {
            return 1;
        }

        private static void PrepareCommand(IDbCommand cmd, IDbConnection conn, IDbTransaction trans, string cmdText, IDataParameter[] cmdParms, int Times = 30)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            cmd.Connection = conn;
            cmd.CommandText = cmdText;
            if (trans != null)
                cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;//cmdType;
            if (Times > 0)//（以秒为单位）。默认为 30 秒
            {
                cmd.CommandTimeout = Times;
            }
            if (cmdParms != null && cmdParms.Length > 0)
            {
                foreach (IDataParameter parameter in cmdParms)
                {
                    if ((parameter.Direction == ParameterDirection.InputOutput
                        || parameter.Direction == ParameterDirection.Input)
                        && (parameter.Value == null))
                    {
                        parameter.Value = DBNull.Value;
                    }
                    cmd.Parameters.Add(parameter);
                }
            }
        }
    }
    public class HelperMsSql : DbHelper
    {
        private static DbConnection GetConn = null;

        public HelperMsSql()
        {

        }

        public override Dictionary<string, string> GetTables()
        {
            throw new NotImplementedException();
        }

        public override List<string> GetAllColumnNames(string tableName)
        {
            throw new NotImplementedException();
        }

        public override bool TabExists(string tableName)
        {
            throw new NotImplementedException();
        }

        public override bool Exists(string strSql, params IDataParameter[] cmdParms)
        {
            throw new NotImplementedException();
        }

        public override T GetSingle<T>(string strSql, int Times = 30, params IDataParameter[] cmdParms)
        {
            throw new NotImplementedException();
        }

        public override DataRow QueryRow(string strSql, int Times = 30, params IDataParameter[] cmdParms)
        {

            //using (IDbConnection connection = )
            //{
            //    using (DbCommand cmd = new SqlCommand())
            //    {
            //        try
            //        {
            //            PrepareCommand(cmd, connection, null, SQLString, cmdParms, Times);
            //            object obj = cmd.ExecuteScalar();
            //            cmd.Parameters.Clear();
            //            return (T)Convert.ChangeType(obj, typeof(T));
            //        }
            //        catch (Exception ex)
            //        {
            //            connection.Close();

            //            throw ex;
            //        }
            //        finally
            //        {
            //            cmd.Dispose();
            //            connection.Close();
            //        }
            //    }
            //}
            throw new NotImplementedException();
        }

        public override DataTable QueryTable(string strSql, int Times = 30, params IDataParameter[] cmdParms)
        {
            throw new NotImplementedException();
        }

        public override DataSet QueryDataSet(string SQLString, int Times = 30, params IDataParameter[] cmdParms)
        {
            throw new NotImplementedException();
        }
    }
}
