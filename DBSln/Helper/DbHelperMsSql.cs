﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;

namespace DBSln.Helper
{
    public class DbHelperMsSql : DBHelper
    {
        public string connectionString = string.Empty;

        public DbHelperMsSql()
        {
            this.connectionString = ConfigurationManager.AppSettings["msConnectionstrings"];
        }

        public DbHelperMsSql(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public override List<string> GetTableNames()
        {
            string sel_sql = "SELECT Name FROM SysObjects Where XType='U' ORDER BY Name";
            DataTable data = QueryTable(sel_sql);
            List<string> lstTableNames = DataUtil.GetFirstCol<string>(data);
            return lstTableNames;
        }

        public override List<string> GetAllColumnNames(string tableName)
        {
            string sel_sql = "SELECT Name FROM SysColumns WHERE id=Object_Id('" + tableName + "') ";
            DataTable data = QueryTable(sel_sql);
            List<string> lstTableNames = DataUtil.GetFirstCol<string>(data);
            return lstTableNames;
        }

        public override bool TabExists(string tableName)
        {
            return base.TabExists(tableName);
        }

        public override bool Exists(string strSql, params System.Data.IDataParameter[] cmdParms)
        {
            throw new NotImplementedException();
        }

        public override T GetSingle<T>(string strSql, int Times = 30, params System.Data.IDataParameter[] cmdParms)
        {
            throw new NotImplementedException();
        }

        public override System.Data.DataRow QueryRow(string strSql, int Times = 30, params System.Data.IDataParameter[] cmdParms)
        {
            throw new NotImplementedException();
        }

        public override System.Data.DataTable QueryTable(string strSql, int Times = 30, params System.Data.IDataParameter[] cmdParms)
        {
            throw new NotImplementedException();
        }

        public override System.Data.DataSet QueryDataSet(string SQLString, int Times = 30, params System.Data.IDataParameter[] cmdParms)
        {
            throw new NotImplementedException();
        }

    }
}
