﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Odbc;
using System.Linq;
using System.Text;

namespace DBProviderNameTest
{
    class Program
    {
        static void Main(string[] args)
        {

            string dbProviderName = "System.Data.Odbc";
            dbProviderName = "System.Data.OleDb";
            dbProviderName = "System.Data.SqlClient";
            dbProviderName = "System.Data.OracleClient";

            //引用 System.Data.SQLite.dll
            dbProviderName = "System.Data.SQLite";

            //引用 MySql.Data.dll
            dbProviderName = "MySql.Data.MySqlClient";

            //引用 Oracle.ManagedDataAccess.dll
            dbProviderName = "Oracle.ManagedDataAccess.Client";


            DbProviderFactory dbfactory = DbProviderFactories.GetFactory(dbProviderName);
            DbConnection dbconn = dbfactory.CreateConnection();

            dbconn.ConnectionString = string.Empty;//提供 各种 数据库连接字符串

            
        }
    }
}
